import random
class Chess():
    def __init__(self, coordinates):
        self.c = coordinates
def checkMoveElefant(self):
        locate_x, locate_y, move_x, move_y = self.c[0],self.c[1], self.c[2], self.c[3]
        if locate_x == move_x or locate_y == move_y:
            return 'no'
        else:
            if abs(int(locate_x) - int(move_x)) == abs(int(locate_y) - int(move_y)):
                return 'yes'
            else:
                return 'no'
def checkMoveQueen(self):
        locate_x, locate_y, move_x, move_y = self.c[0], self.c[1], self.c[2], self.c[3]
        if locate_x == move_x and locate_y == move_y:
            return 'no'
        else:
            if (abs(int(locate_x) - int(move_x)) == abs(int(locate_y) - int(move_y))) or (locate_x == move_x or locate_y == move_y):
                return 'yes'
            else:
                return 'no'
def checkMoveHorse(self):
        locate_x, locate_y, move_x, move_y = self.c[0], self.c[1], self.c[2], self.c[3]
        if locate_x == move_x and locate_y == move_y:
            return 'no'
        else:
            if abs(int(locate_x) - int(move_x)) == 2 and abs(int(locate_y) - int(move_y)) == 1 or abs(int(locate_y) - int(move_y)) == 2 and abs(int(locate_x) - int(move_x)) == 1:
                return 'yes'
            else:
                return 'no'

class GCD():
    def check(n1, n2):
        if n2 > n1:
            (n1, n2) = (n2, n1)
        if (n1 % n2) == 0:
            return f'GCD is {n2}'
        else:
            return (GCD.check(n2, n1 % n2))

from random import randint


class FileTasks:
    def task1(self):
        with open('input.txt') as f1, open('output.txt', 'w+') as f2:
            for line in f1:
                if (int(''.join(line.split(', ')[2].split(':'))) - int(''.join(line.split(', ')[1].split(':')))) - 40 >= 60:
                    f2.write(line.split(', ')[0] + ' ')
    def task2(self):
        with open('class_scores') as cs, open('new_scores', 'w+') as ns:
            for line in cs:
                if int(line.split()[1]) + 5 <= 100:
                    ns.write(line.split()[0] + ' ' + str(int(line.split()[1]) + 5) + '\n')
                else:
                    ns.write(line.split()[0] + ' ' + str(100) + '\n')
    def task3(self):
        with open('random.txt', 'w+') as rndm:
            for i in range(25):
                rndm.write(str(randint(111, 777)) + '\n')
    def task4(self):
        with open('data.csv') as file:
            first_line = file.readline().strip().split(',')
            another_line = [line.strip().split(',') for line in file]
            in_res = {}
            res = []
            for i in range(len(another_line)):
                for j in range(len(another_line[i])):
                    in_res.update({first_line[j]: another_line[i][j]})
                res.append(in_res)
                in_res = {}
            return res
    def task5(self):
        with open('name') as name, open('surname') as surname:
            names = name.readlines()
            surnames = surname.readlines()
            return f'{random.choice(names).strip()} {random.choice(surnames).strip()}'
